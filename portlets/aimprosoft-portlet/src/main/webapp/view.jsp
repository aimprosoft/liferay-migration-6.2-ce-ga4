<%@ page import="com.aimprosoft.demo.model.AssetDTO" %>
<%@ page import="com.aimprosoft.demo.model.ContentDTO" %>
<%@ page import="com.liferay.portal.kernel.util.FastDateFormatFactoryUtil" %>
<%@ page import="java.text.Format" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Locale" %>
<%@ taglib uri="http://java.sun.com/portlet_2_0" prefix="portlet" %>
<%@ taglib uri="http://liferay.com/tld/aui" prefix="aui" %>
<portlet:defineObjects/>
<div class="aimprosoft-wrapper">
    <%
    Format dateFormatDateTime = FastDateFormatFactoryUtil.getDate(Locale.ENGLISH);
    ContentDTO content = (ContentDTO) request.getAttribute("content");

    List<AssetDTO> wcs = content.getWcs();
    if (wcs.size() > 0) {
    %>
        <h3>Web Contents</h3>
        <table class="table table-bordered table-hover table-striped">
            <thead class="table-columns">
                <tr>
                    <th class="table-first-header">#</th>
                    <th>Name</th>
                    <th>Community</th>
                    <th>Author</th>
                    <th>Create date</th>
                    <th class="table-last-header">Modify date</th>
                </tr>
            </thead>
            <tbody class="table-data">
            <% for (AssetDTO wc : wcs) { %>
                <tr>
                    <td class="table-cell first"><%= wc.getClassPK() %></td>
                    <td class="table-cell"><%= wc.getName() %></td>
                    <td class="table-cell"><%= wc.getCommunity() %></td>
                    <td class="table-cell"><%= wc.getAuthor() %></td>
                    <td class="table-cell"><%= dateFormatDateTime.format(wc.getCreateDate()) %></td>
                    <td class="table-cell last"><%= dateFormatDateTime.format(wc.getModifyDate()) %></td>
                </tr>
            <% } %>
            </tbody>
        </table>
    <%
    }

    List<AssetDTO> docs = content.getDocs();
    if (docs.size() > 0) {
        %>
        <h3>Documents</h3>
        <table class="table table-bordered table-hover table-striped">
            <thead class="table-columns">
            <tr>
                <th class="table-first-header">#</th>
                <th>Name</th>
                <th>Community</th>
                <th>Author</th>
                <th>Create date</th>
                <th class="table-last-header">Modify date</th>
            </tr>
            </thead>
            <tbody class="table-data">
            <% for (AssetDTO doc : docs) { %>
            <tr>
                <td class="table-cell first"><%= doc.getClassPK() %></td>
                <td class="table-cell"><%= doc.getName() %></td>
                <td class="table-cell"><%= doc.getCommunity() %></td>
                <td class="table-cell"><%= doc.getAuthor() %></td>
                <td class="table-cell"><%= dateFormatDateTime.format(doc.getCreateDate()) %></td>
                <td class="table-cell last"><%= dateFormatDateTime.format(doc.getModifyDate()) %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
        <%
    }

    List<AssetDTO> blogs = content.getBlogs();
    if (blogs.size() > 0) {
    %>
        <h3>Blogs</h3>
        <table class="table table-bordered table-hover table-striped">
            <thead class="table-columns">
            <tr>
                <th class="table-first-header">#</th>
                <th>Name</th>
                <th>Community</th>
                <th>Author</th>
                <th>Create date</th>
                <th class="table-last-header">Modify date</th>
            </tr>
            </thead>
            <tbody class="table-data">
            <% for (AssetDTO blog : blogs) { %>
            <tr>
                <td class="table-cell first"><%= blog.getClassPK() %></td>
                <td class="table-cell"><%= blog.getName() %></td>
                <td class="table-cell"><%= blog.getCommunity() %></td>
                <td class="table-cell"><%= blog.getAuthor() %></td>
                <td class="table-cell"><%= dateFormatDateTime.format(blog.getCreateDate()) %></td>
                <td class="table-cell last"><%= dateFormatDateTime.format(blog.getModifyDate()) %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
    <%
    }

    List<AssetDTO> wikis = content.getWikis();
    if (wikis.size() > 0) {
    %>
        <h3>Wikis</h3>
        <table class="table table-bordered table-hover table-striped">
            <thead class="table-columns">
            <tr>
                <th class="table-first-header">#</th>
                <th>Name</th>
                <th>Community</th>
                <th>Author</th>
                <th>Create date</th>
                <th class="table-last-header">Modify date</th>
            </tr>
            </thead>
            <tbody class="table-data">
            <% for (AssetDTO wiki : wikis) { %>
            <tr>
                <td class="table-cell first"><%= wiki.getClassPK() %></td>
                <td class="table-cell"><%= wiki.getName() %></td>
                <td class="table-cell"><%= wiki.getCommunity() %></td>
                <td class="table-cell"><%= wiki.getAuthor() %></td>
                <td class="table-cell"><%= dateFormatDateTime.format(wiki.getCreateDate()) %></td>
                <td class="table-cell last"><%= dateFormatDateTime.format(wiki.getModifyDate()) %></td>
            </tr>
            <% } %>
            </tbody>
        </table>
    <%
    }
    %>
</div>