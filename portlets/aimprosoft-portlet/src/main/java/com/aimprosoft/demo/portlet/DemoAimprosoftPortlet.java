package com.aimprosoft.demo.portlet;

import com.aimprosoft.demo.model.AssetDTO;
import com.aimprosoft.demo.model.ContentDTO;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;
import com.liferay.portal.service.UserLocalServiceUtil;
import com.liferay.portlet.blogs.model.BlogsEntry;
import com.liferay.portlet.blogs.service.BlogsEntryLocalServiceUtil;
import com.liferay.portlet.documentlibrary.model.DLFileEntry;
import com.liferay.portlet.documentlibrary.service.DLFileEntryLocalServiceUtil;
import com.liferay.portlet.journal.model.JournalArticle;
import com.liferay.portlet.journal.service.JournalArticleLocalServiceUtil;
import com.liferay.portlet.messageboards.model.MBMessage;
import com.liferay.portlet.messageboards.service.MBMessageLocalServiceUtil;
import com.liferay.portlet.wiki.model.WikiPage;
import com.liferay.portlet.wiki.service.WikiPageLocalServiceUtil;
import com.liferay.util.bridges.mvc.MVCPortlet;

import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DemoAimprosoftPortlet extends MVCPortlet {

    @Override
    public void render(RenderRequest request, RenderResponse response) throws PortletException, IOException {

        List<AssetDTO> wcs = getWebContents();
        List<AssetDTO> docs = getDLDocuments();
        List<AssetDTO> wikis = getWikis();
        List<AssetDTO> blogs = getBlogs();

        ContentDTO contentDTO = new ContentDTO(wcs, docs, wikis, blogs);
        request.setAttribute(CONTENT, contentDTO);

        super.render(request, response);
    }

    private List<AssetDTO> getWebContents() {
        List<AssetDTO> wcs = new ArrayList<AssetDTO>();
        try {
            List<JournalArticle> articles = JournalArticleLocalServiceUtil.getArticles();
            for (JournalArticle article : articles) {

                AssetDTO wc = new AssetDTO();

                long userId = article.getUserId();
                String userName = getUserName(userId);
                wc.setAuthor(userName);

                String title = article.getTitle(Locale.ENGLISH);
                wc.setName(title);

                long groupId = article.getGroupId();
                String groupName = getGroupName(groupId);
                wc.setCommunity(groupName);

                wc.setClassPK(article.getId());
                wc.setCreateDate(article.getCreateDate());
                wc.setModifyDate(article.getModifiedDate());

                wcs.add(wc);
            }
        } catch (Exception e) {
            _log.error(e, e);
        }
        return wcs;
    }


    private List<AssetDTO> getDLDocuments() {
        List<AssetDTO> docs = new ArrayList<AssetDTO>();
        try {
            List<DLFileEntry> fileEntries = DLFileEntryLocalServiceUtil.getFileEntries(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            for (DLFileEntry entry : fileEntries) {

                AssetDTO doc = new AssetDTO();

                long userId = entry.getUserId();
                String userName = getUserName(userId);
                doc.setAuthor(userName);

                String title = entry.getTitle();
                doc.setName(title);

                long groupId = entry.getGroupId();
                String groupName = getGroupName(groupId);
                doc.setCommunity(groupName);

                doc.setClassPK(entry.getFileEntryId());
                doc.setCreateDate(entry.getCreateDate());
                doc.setModifyDate(entry.getModifiedDate());

                docs.add(doc);
            }
        } catch (Exception e) {
            _log.error(e, e);
        }
        return docs;
    }


    private List<AssetDTO> getWikis() {
        List<AssetDTO> wikis = new ArrayList<AssetDTO>();
        try {
            List<WikiPage> wikiPages = WikiPageLocalServiceUtil.getWikiPages(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            for (WikiPage page : wikiPages) {

                AssetDTO wiki = new AssetDTO();

                long userId = page.getUserId();
                String userName = getUserName(userId);
                wiki.setAuthor(userName);

                String title = page.getTitle();
                wiki.setName(title);

                long groupId = page.getGroupId();
                String groupName = getGroupName(groupId);
                wiki.setCommunity(groupName);

                wiki.setClassPK(page.getPageId());
                wiki.setCreateDate(page.getCreateDate());
                wiki.setModifyDate(page.getModifiedDate());

                wikis.add(wiki);
            }
        } catch (Exception e) {
            _log.error(e, e);
        }
        return wikis;
    }

    private List<AssetDTO> getBlogs() {
        List<AssetDTO> blogs = new ArrayList<AssetDTO>();
        try {

            List<BlogsEntry> blogsEntries = BlogsEntryLocalServiceUtil.getBlogsEntries(QueryUtil.ALL_POS, QueryUtil.ALL_POS);
            for (BlogsEntry entry : blogsEntries) {

                AssetDTO blog = new AssetDTO();

                long userId = entry.getUserId();
                String userName = getUserName(userId);
                blog.setAuthor(userName);

                String title = entry.getTitle();
                blog.setName(title);

                long groupId = entry.getGroupId();
                String groupName = getGroupName(groupId);
                blog.setCommunity(groupName);

                blog.setClassPK(entry.getEntryId());
                blog.setCreateDate(entry.getCreateDate());
                blog.setModifyDate(entry.getModifiedDate());

                blogs.add(blog);
            }

        } catch (Exception e) {
            _log.error(e, e);
        }
        return blogs;
    }

    private String getGroupName(long groupId) throws PortalException, SystemException {
        Group group = GroupLocalServiceUtil.getGroup(groupId);
        return group.getName();
    }

    private String getUserName(long authorUserId) throws PortalException, SystemException {
        User user = UserLocalServiceUtil.getUser(authorUserId);
        return user.getFullName();
    }

    private static final String CONTENT = "content";

    private static Log _log = LogFactoryUtil.getLog(DemoAimprosoftPortlet.class);
}