package com.aimprosoft.demo.model;

import java.util.ArrayList;
import java.util.List;

public class ContentDTO {

    private List<AssetDTO> wcs = new ArrayList<AssetDTO>();
    private List<AssetDTO> docs = new ArrayList<AssetDTO>();
    private List<AssetDTO> wikis = new ArrayList<AssetDTO>();
    private List<AssetDTO> blogs = new ArrayList<AssetDTO>();

    public ContentDTO(List<AssetDTO> wcs, List<AssetDTO> docs, List<AssetDTO> wikis, List<AssetDTO> blogs) {
        this.wcs = wcs;
        this.docs = docs;
        this.wikis = wikis;
        this.blogs = blogs;
    }

    public List<AssetDTO> getWcs() {
        return wcs;
    }

    public void setWcs(List<AssetDTO> wcs) {
        this.wcs = wcs;
    }

    public List<AssetDTO> getDocs() {
        return docs;
    }

    public void setDocs(List<AssetDTO> docs) {
        this.docs = docs;
    }

    public List<AssetDTO> getWikis() {
        return wikis;
    }

    public void setWikis(List<AssetDTO> wikis) {
        this.wikis = wikis;
    }

    public List<AssetDTO> getBlogs() {
        return blogs;
    }

    public void setBlogs(List<AssetDTO> blogs) {
        this.blogs = blogs;
    }
}
